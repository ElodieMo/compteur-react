# compteur-react

Contexte du projet

La mairie de Toulouse souhaite développer une application très simple permettant de compter les participants d'activités diverses (match, festival, vote, etc…).

​

La principe de l'application est simple, elle doit comporter :

​

    un compteur
    un bouton permettant d'incrémenter le compteur
    un bouton permettant de décrémenter le compteur (il ne doit pas aller en dessous de zéro)
    un bouton permettant de remettre le compteur à zéro
    Une fois l'application développée, partagé votre code versionné sur Gitlab

​

Tu peux t'aider du compteur créé par défaut lors de l'initialisation d'un projet avec Vite
