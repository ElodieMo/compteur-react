import { useState } from 'react'
import './App.css'

function App() {
  const [count, setCount] = useState(0)
function increment(){
count +1
};
function decrement() {
  count -1
};
  return (
    <div className="App">
      <h1>Compteur</h1>
      <div className='Compteur'>
        <button onClick={() => setCount((count) => count + 1)}>
          +{count}
        </button>
        
        <button onClick={() => setCount((count) => count >0 ? count -1 : count =0)}>
      -</button>
      <button onClick={() => setCount((count) => count = 0)}>
      reset</button></div></div>
  )
}

export default App
